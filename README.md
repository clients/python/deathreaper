# DeathReaper

Repository using Silkaj image to publish Ğ1 and Ğ1-test exclusions on Discourse Forums.
Scheduled at 9 AM and 9 PM.

- [fr] [Je me présente je suis DeathReaper, alias La Faucheuse](https://forum.duniter.org/t/je-me-presente-je-suis-deathreaper-alias-la-faucheuse/6539)

Published for:
- Ğ1:
  - [Duniter Forum](https://forum.duniter.org/t/exclusions-de-la-toile-de-confiance-g1-expirations-des-statuts-de-membre/4393/last)
  - [Monnaie Libre Forum: Part 4](https://forum.monnaie-libre.fr/t/exclusions-de-la-toile-de-confiance-g1-expirations-des-statuts-de-membre-partie-4/30219/last)
  - [Monnaie Libre Forum: Part 3](https://forum.monnaie-libre.fr/t/exclusions-de-la-toile-de-confiance-g1-expirations-des-statuts-de-membre-partie-3/26117/last)
  - [Monnaie Libre Forum: Part 2](https://forum.monnaie-libre.fr/t/exclusions-de-la-toile-de-confiance-g1-expirations-des-statuts-de-membre-partie-2/17627/last)
  - [Monnaie Libre Forum: Part 1](https://forum.monnaie-libre.fr/t/exclusions-de-la-toile-de-confiance-g1-expirations-des-statuts-de-membre/8233/last)
- Ğ1-Test:
  - [Duniter Forum](https://forum.duniter.org/t/exclusions-de-la-toile-de-confiance-g1-test-expirations-des-statuts-de-membre/6554/last)

### How to publish on the Forums?
`PUBLISH` variable should be set to `--publish`.

<!-- Not necessary, since the publication is not happening without `\-\-publish` flag
### How to push new commit by not triggering the harvest?
- [Found](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/2496#note_264823877)
```sh
git push -o ci.skip
```
-->

### Logo
- [From Commons](https://commons.wikimedia.org/wiki/File:Creative-Tail-Halloween-death.svg)

### License
Publish under GNU AGPLv3.
